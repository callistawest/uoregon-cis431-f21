#include <stdlib.h> 
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include "common.h"
#include <inttypes.h>
#include <time.h>


#define PI 3.1415926535


void usage(int argc, char** argv);
double calcPi_Serial(int num_steps);
double calcPi_P1(int num_steps);
double calcPi_P2(int num_steps);

int main(int argc, char** argv)
{
    // get input values
    uint32_t num_steps = 100000;
    if(argc > 1) {
        num_steps = atoi(argv[1]);
    } else {
        usage(argc, argv);
        printf("using %"PRIu32"\n", num_steps);
    }
    fprintf(stdout, "The first 10 digits of Pi are %0.10f\n", PI);


    // set up timer
    uint64_t start_t;
    uint64_t end_t;
    InitTSC();


    // calculate in serial 
    start_t = ReadTSC();
    double Pi0 = calcPi_Serial(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi serially with %"PRIu32" steps is: %g\n",
           num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %0.10f\n", Pi0);
    
    // calculate in parallel with reduce 
    start_t = ReadTSC();
    double Pi1 = calcPi_P1(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi in // with %"PRIu32" steps is: %g\n",
           num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %0.10f\n", Pi1);


    // calculate in parallel with atomic add
    start_t = ReadTSC();
    double Pi2 = calcPi_P2(num_steps);
    end_t = ReadTSC();
    printf("Time to calculate Pi in // + atomic with %"PRIu32" steps is: %g\n",
           num_steps, ElapsedTime(end_t - start_t));
    printf("Pi is %0.10f\n", Pi2);

    
    return 0;
}


void usage(int argc, char** argv)
{
    fprintf(stdout, "usage: %s <# steps>\n", argv[0]);
}

double calcPi_Serial(int num_steps)
{
    //following the Gregory-Leibniz equation
    double pi = 0.0;
    double factor = 1.0;
    for (int i = 0; i < num_steps; i++)
    {
        pi += (factor/(2.0 * i + 1.0));
        factor = -1.0 * factor;         //factor variable gets multiplied by -1 to account for odd index term
    }
    return pi*4.0;              //from equation of mutilplying summantion by 4
}

double monteCarlo_Serial(int num_steps)
{
    double pi = 0.0;
    int inCircle = 0;
    double x_coor = 0;
    double y_coor = 0;
    double distance = 0.0;
    for (int i = 0; i < num_steps; i++)
    {
        x_coor = (double)rand()/ RAND_MAX;      //find random coordinate between 0 & 1
        y_coor = (double)rand() / RAND_MAX;
        distance = (pow(x_coor, 2) + pow(y_coor,2));    //find distance to see if in circle
        if (distance <= 1)
        {
            inCircle += 1;
        }
    }
    pi = (double)inCircle/num_steps * 4;        //must convert inCircle to double
    return pi;
}

double calcPi_P1(int num_steps)
{
    //following the Gregory-Leibniz equation
    double pi = 0.0;
    double factor = 1.0;
    omp_set_num_threads(4);
    //#pragma omp parallel for private(factor) reduction(+: pi) 
    //#pragma omp parallel for shared(num_steps, factor) reduction(+: pi) 
    #pragma omp parallel for reduction(+: pi) 
    for (int i = 0; i < num_steps; i++)
    {
        pi += (factor/(2.0 * i + 1.0));
        factor = -1.0 * factor;         //factor variable gets multiplied by -1 to account for odd index term
    }
    if (pi < 0) {
        pi = pi * -1;
    }
    return pi*4.0;              //from equation of mutilplying summantion by 4

}

double calcPi_P2(int num_steps)
{
    double pi = 0.0;
    int i;
    double factor = 1.0/num_steps;
    double x = 0.0;
    omp_set_num_threads(4);
    #pragma omp parallel shared(num_steps, factor)
    {
        double area_thrd = 0;
        #pragma omp for
        for (i = 0; i < num_steps; i++)
        {
            x = factor * (i - 0.5);
            area_thrd += (4.0/(1.0 + x*x));
        }
        #pragma omp atomic
        pi += area_thrd;
    }
    return pi * factor;

}
