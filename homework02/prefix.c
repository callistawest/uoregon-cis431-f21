#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
#include "common.h"


void usage(int argc, char** argv);
void verify(int* sol, int* ans, int n);
void prefix_sum(int* src, int* prefix, int n);
void prefix_sum_p1(int* src, int* prefix, int n);
void prefix_sum_p2(int* src, int* prefix, int n);


int main(int argc, char** argv)
{
    // get inputs
    uint32_t n = 1048576;
    //testing values of n for small data sets for accuracy
    //uint32_t n = 10;
    unsigned int seed = time(NULL);
    if(argc > 2) {
        n = atoi(argv[1]);
        seed = atoi(argv[2]);
    } else {
        usage(argc, argv);
        printf("using %"PRIu32" elements and time as seed\n", n);
    }


    // set up data 
    int* prefix_array = (int*) AlignedMalloc(sizeof(int) * n);
    int* input_array = (int*) AlignedMalloc(sizeof(int) * n);
    srand(seed);
    for(int i = 0; i < n; i++) {
        input_array[i] = rand() % 100;
    }


    // set up timers
    uint64_t start_t;
    uint64_t end_t;
    InitTSC();

    // execute serial prefix sum and use it as ground truth
    start_t = ReadTSC();
    prefix_sum(input_array, prefix_array, n);
    end_t = ReadTSC();
    printf("Time to do O(N-1) prefix sum on a %"PRIu32" elements: %g (s)\n",
           n, ElapsedTime(end_t - start_t));


    // execute parallel prefix sum which uses a NlogN algorithm
    int* input_array1 = (int*) AlignedMalloc(sizeof(int) * n);
    int* prefix_array1 = (int*) AlignedMalloc(sizeof(int) * n);
    memcpy(input_array1, input_array, sizeof(int) * n);
    start_t = ReadTSC();
    prefix_sum_p1(input_array1, prefix_array1, n);
    end_t = ReadTSC();
    printf("Time to do O(NlogN) //prefix sum on a %"PRIu32" elements: %g (s)\n",
           n, ElapsedTime(end_t - start_t));
    verify(prefix_array, prefix_array1, n);


    // execute parallel prefix sum which uses a 2(N-1) algorithm
    memcpy(input_array1, input_array, sizeof(int) * n);
    memset(prefix_array1, 0, sizeof(int) * n);
    start_t = ReadTSC();
    prefix_sum_p2(input_array1, prefix_array1, n);
    end_t = ReadTSC();
    printf("Time to do 2(N-1) //prefix sum on a %"PRIu32" elements: %g (s)\n",
           n, ElapsedTime(end_t - start_t));
    verify(prefix_array, prefix_array1, n);


    // free memory
    AlignedFree(prefix_array);
    AlignedFree(input_array);
    AlignedFree(input_array1);
    AlignedFree(prefix_array1);


    return 0;
}

void usage(int argc, char** argv)
{
    fprintf(stderr, "usage: %s <# elements> <rand seed>\n", argv[0]);
}

void verify(int* sol, int* ans, int n)
{
    int err = 0;
    for(int i = 0; i < n; i++) {
        if(sol[i] != ans[i]) {
            err++;
        }
    }
    if(err != 0) {
        fprintf(stderr, "There was an error: %d\n", err);
    } else {
        fprintf(stdout, "Pass\n");
    }
}

void prefix_sum(int* src, int* prefix, int n)
{
    prefix[0] = src[0];
    for(int i = 1; i < n; i++) {
        prefix[i] = src[i] + prefix[i - 1];
    }
}

void prefix_sum_p2(int* src, int* prefix, int n)
{

    prefix[0] = src[0];
    omp_set_num_threads(8);
    //initalizing new variable using prefix array
    int numthreads, *prefix_2, *prefix_1 = prefix;
    //parallized method for prefix sum using openMP
    #pragma omp parallel
    {
        int i;
        //must be ran by a single available thread
        #pragma omp single
        {
            numthreads = omp_get_num_threads();
            //printf("numthreads %d\n", numthreads);
            prefix_2 = malloc(sizeof(int)*numthreads+1);
            prefix_2[0] = 0;
        }
        int tid = omp_get_thread_num();
        int sum = 0;
        //up sweep of tree
        #pragma omp for schedule(static) 
        for(i=0; i<n; i++)
        {
            sum += src[i];
            prefix_1[i] = sum;
        }
        //stores the sum of each thread in the thread number index of prefix_2
        //can be accesses by multiple threads
        prefix_2[tid+1] = sum;
        //down sweep of tree
        #pragma omp barrier
        int tmp = 0;
        for(i=0; i<(tid+1); i++)
        {
            tmp += prefix_2[i];
        }
        //for loop to copy thread sum to indicies in prefix_1 array 
        #pragma omp for schedule(static)
        for(i=0; i<n; i++)
        {
            prefix_1[i] += tmp;
        }
    }
    free(prefix_2);
}

void prefix_sum_p1(int* src, int* prefix, int n)
{
    int x, i;
    omp_set_num_threads(8);
    prefix[0] = src[0];
    for(int j = 0; j < (log(n)/log(2)); j++)
    {
        //declares data to have a separate copy in the memory of each thread
        #pragma omp parallel private(i)
        {
            #pragma omp for
            for(i = 1<<j; i < n; i++)
            {
                //bitwise shift on on j to create a bit mask so the j-th bit is set
                prefix[i] = src[i] + src[i-(1<<j)];
            }
            #pragma omp for
            for(int i = 1<<j; i < n; i++)
            {
                src[i] = prefix[i];
                //copy back to src array for each value of i
            }

        }
    }
}
